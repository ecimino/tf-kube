#!/bin/bash

set -x

sudo yum update -y \
    && sudo yum upgrade -y

sudo yum install -y jq

sudo amazon-linux-extras install -y docker \
    && sudo systemctl enable --now docker \
    && sudo usermod ec2-user -aG docker

# custom bashrc
aws s3 cp s3://game-bootstrap/bashrc /home/ec2-user/
cat /home/ec2-user/bashrc >> /home/ec2-user/.bashrc
source /home/ec2-user/.bashrc


sleep 3

mkdir -p /tf2-data


if_fs=$(sudo file -s /dev/nvme1n1 | grep -v XFS)

if [ "$if_fs" ]; then
    sudo mkfs -t xfs /dev/nvme1n1 || exit 1
fi

sudo mount /dev/nvme1n1 /tf2-data
chmod 777 /tf2-data


aws s3 cp s3://game-bootstrap/server_cfg /tf2-data/tf/cfg/server.cfg
chmod 644 /tf2-data/tf/cfg/server.cfg

aws s3 cp s3://game-bootstrap/mapcycle /tf2-data/tf/cfg/mapcycle.txt
chmod 644 /tf2-data/tf/cfg/mapcycle.txt

# docker run -d --net=host --name=tf2 --env_file /home/ec2-user/.server_env_vars -v /tf2-data:/home/steam/tf-dedicated/ cm2network/tf2 
docker run -d --net=host --name=tf2 \
    -e SRCDS_PW=$(aws ssm get-parameter  --region us-east-2 --name SRCDS_PW | jq -r '.Parameter.Value') \
    -e SRCDS_RCONPW=$(aws ssm get-parameter --region us-east-2 --name SRCDS_RCONPW | jq -r '.Parameter.Value') \
    -e SRCDS_TOKEN=$(aws ssm get-parameter --region us-east-2 --name SRCDS_TOKEN | jq -r '.Parameter.Value') \
    -e SRCDS_WORKSHOP_START_MAP=$(aws ssm get-parameter  --region us-east-2 --name SRCDS_WORKSHOP_START_MAP | jq -r '.Parameter.Value') \
    -v /tf2-data:/home/steam/tf-dedicated/ cm2network/tf2 


curl -X POST -H 'Content-type: application/json' --data '{"text":"✅ terraform apply complete ✅"}' https://hooks.slack.com/services/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