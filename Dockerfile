# eventually may want to re-make this Dockerfile 
# https://hub.docker.com/r/cm2network/tf2/tags?page=1&ordering=last_updated



# 1
# ADD file ... in /
# 25.84 MB
# 2
# CMD ["bash"]
# 0 B
# 3
# LABEL maintainer=walentinlamonos@gmail.com
# 0 B
# 4
# ARG PUID=1000
# 0 B
# 5
# ENV USER=steam
# 0 B
# 6
# ENV HOMEDIR=/home/steam
# 0 B
# 7
# ENV STEAMCMDDIR=/home/steam/steamcmd
# 0 B
# 8
# |1 PUID=1000 /bin/sh -c set
# 143.25 MB
# 9
# WORKDIR /home/steam/steamcmd
# 0 B
# 10
# VOLUME [/home/steam/steamcmd]
# 0 B
# 11
# LABEL maintainer=walentinlamonos@gmail.com
# 0 B
# 12
# ENV STEAMAPPID=232250
# 0 B
# 13
# ENV STEAMAPP=tf
# 0 B
# 14
# ENV STEAMAPPDIR=/home/steam/tf-dedicated
# 0 B
# 15
# ENV DLURL=https://raw.githubusercontent.com/CM2Walki/TF2
# 0 B
# 16
# /bin/sh -c set -x &&
# 7.8 MB
# 17
# ENV SRCDS_FPSMAX=300 SRCDS_TICKRATE=66 SRCDS_PORT=27015
# 0 B
# 18
# USER steam
# 0 B
# 19
# VOLUME [/home/steam/tf-dedicated]
# 0 B
# 20
# WORKDIR /home/steam
# 0 B
# 21
# CMD ["bash" "entry.sh"]
# 0 B
# 22
# EXPOSE 27015/tcp 27015/udp 27020/udp
# 0 B
# Command
# ADD file:3af3091e7d2bb40bc1e6550eb5ea290badc6bbf3339105626f245a963cc11450 in / 