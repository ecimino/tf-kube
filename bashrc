
# Custom history settings
HISTSIZE=100000
SAVEHIST=100000
HISTCONTROL=ignoredups:erasedups
shopt -s histappend
export PROMPT_COMMAND="history -a; history -c; history -r; $PROMPT_COMMAND" #append history to all open connections to host

# custom search and execute history
if [[ $- == *i* ]]
then
    bind '"\e[A": history-search-backward'
    bind '"\e[B": history-search-forward'
fi
###################################

alias grep='grep --color=auto'
alias egrep='grep -E --color=auto'

alias vimbashrc='vim ~/.bashrc'
alias sourcebashrc='source ~/.bashrc'

# . ~/.git-bash-completion
# . ~/.git-bash-ps1

# Terminal prompt display
#PS1="\h:\w\$ "
PS1='\[\033[38;5;014m\]\t\[\033[0m\] \[\033[38;5;118m\]\h\[\033[0m\] \[\033[38;5;200m\]\w\[\033[0m\]\[\033[38;5;7m\] ⎈ \[\033[0m\]' # 256 colors, lavender/purple


# Terminal colors
#alias ls="ls -Gp"
export CLICOLOR=1
export LSCOLORS=exfxhxdxCxegedabagacad

# source <(kubectl completion bash)
