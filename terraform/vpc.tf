resource "aws_vpc" "main" {
  cidr_block = "10.100.0.0/16"
  tags = {
    Name = "game"
  }
}

resource "aws_subnet" "main" {
  vpc_id                  = aws_vpc.main.id
  cidr_block              = "10.100.1.0/24"
  map_public_ip_on_launch = true
}

resource "aws_internet_gateway" "gw" {
  vpc_id = aws_vpc.main.id

  tags = {
    Name = "game-gw"
  }
}

resource "aws_route_table" "rt" {
  vpc_id = aws_vpc.main.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.gw.id
  }

  tags = {
    Name = "game-rt"
  }
}

resource "aws_route_table_association" "rt-a" {
  subnet_id      = aws_subnet.main.id
  route_table_id = aws_route_table.rt.id
}

resource "aws_security_group" "main" {
  name        = "game-home_access"
  description = "Allow TLS inbound traffic"
  vpc_id      = aws_vpc.main.id

  ingress {
    description = "ssh from home"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["207.181.236.46/32", "68.251.107.113/32"]
  }

  ingress {
    description = "TF2 TCP main port"
    from_port   = 27000
    to_port     = 27100
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "TF2 UDP main port"
    from_port   = 27000
    to_port     = 27100
    protocol    = "udp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # ingress {
  #   description = "UDP steam port 27036"
  #   from_port   = 27036
  #   to_port     = 27036
  #   protocol    = "TCP"
  #   cidr_blocks = ["0.0.0.0/0"]
  # }

  ingress {
    description = "UDP steam port 26091"
    from_port   = 26091
    to_port     = 26091
    protocol    = "udp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
