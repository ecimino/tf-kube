resource "aws_iam_instance_profile" "game-s3" {
  name = "game-s3"
  role = aws_iam_role.game-role.name
}

resource "aws_iam_role" "game-role" {
  name = "game-role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_policy" "s3-read-write" {
  name = "s3-read-write"
  #   role = aws_iam_role.game-role.id

  # Terraform's "jsonencode" function converts a
  # Terraform expression result to valid JSON syntax.
  policy = jsonencode({
    "Version" : "2012-10-17",
    "Statement" : [
      {
        "Sid" : "VisualEditor0",
        "Effect" : "Allow",
        "Action" : "s3:ListBucket",
        "Resource" : "arn:aws:s3:::*"
      },
      {
        "Sid" : "VisualEditor1",
        "Effect" : "Allow",
        "Action" : [
          "s3:PutObject",
          "s3:GetObject"
        ],
        "Resource" : "arn:aws:s3:::*/*"
      },
      {
        "Sid" : "VisualEditor2",
        "Effect" : "Allow",
        "Action" : [
          "s3:ListAllMyBuckets",
          "s3:HeadBucket"
        ],
        "Resource" : "*"
      }
    ]
  })
}


resource "aws_iam_policy" "ssm-getparams" {
  name = "ssm-getparams"
  #   role = aws_iam_role.game-role.id

  # Terraform's "jsonencode" function converts a
  # Terraform expression result to valid JSON syntax.
  policy = jsonencode({
    "Version" : "2012-10-17",
    "Statement" : [
      {
        "Effect" : "Allow",
        "Action" : [
          "ssm:*"
        ],
        "Resource" : "*"
      },
      {
        "Effect" : "Allow",
        "Action" : [
          "ssm:*"
        ],
        "Resource" : "arn:aws:ssm:*:057774443002:parameter/*"
      }
    ]
  })
}


resource "aws_iam_role_policy_attachment" "game-s3-policy-attach" {
  role       = aws_iam_role.game-role.name
  policy_arn = aws_iam_policy.s3-read-write.arn
}


resource "aws_iam_role_policy_attachment" "game-ssm-policy-attach" {
  role       = aws_iam_role.game-role.name
  policy_arn = aws_iam_policy.ssm-getparams.arn
}