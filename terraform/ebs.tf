resource "aws_ebs_volume" "game-tf2-ebs-volume" {
  availability_zone = "us-east-2a"
  size              = 20

  tags = {
    Name = "tf2"
  }
}