resource "aws_s3_bucket" "game-bootstrap" {
  bucket        = "game-bootstrap"
  acl           = "private"
  force_destroy = true
}

resource "aws_s3_bucket_object" "bashrc" {
  key    = "bashrc"
  bucket = aws_s3_bucket.game-bootstrap.id
  source = "../bashrc"
}

resource "aws_s3_bucket_object" "server_cfg" {
  key    = "server_cfg"
  bucket = aws_s3_bucket.game-bootstrap.id
  source = "../tf2-env/server.cfg"
}

resource "aws_s3_bucket_object" "mapcycle" {
  key    = "mapcycle"
  bucket = aws_s3_bucket.game-bootstrap.id
  source = "../tf2-env/mapcycle.txt"
}
