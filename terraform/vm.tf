resource "aws_key_pair" "deployer" {
  key_name   = "deployer-key"
  public_key = file("~/.ssh/aws-node.pub")
}

resource "aws_instance" "main" {
  availability_zone           = "us-east-2a"
  depends_on                  = [aws_s3_bucket_object.bashrc]
  key_name                    = aws_key_pair.deployer.key_name
  ami                         = "ami-0f7919c33c90f5b58"
  instance_type               = "c5.large"
  vpc_security_group_ids      = [aws_security_group.main.id]
  subnet_id                   = aws_subnet.main.id
  associate_public_ip_address = true
  iam_instance_profile        = aws_iam_instance_profile.game-s3.name

  root_block_device {
    volume_size = 8
  }

  user_data = filebase64("../setup.sh")
}

resource "aws_volume_attachment" "game-ebs_att" {
  device_name = "/dev/sdh"
  volume_id   = aws_ebs_volume.game-tf2-ebs-volume.id
  instance_id = aws_instance.main.id
}

resource "aws_route53_record" "public_dns" {
  zone_id = aws_route53_zone.primary.zone_id
  name    = "game.runf11s.com"
  type    = "A"
  ttl     = "60"
  records = [aws_instance.main.public_ip]
}

output "public_ip" {
  value = aws_instance.main.public_ip
}
