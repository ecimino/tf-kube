resource "aws_route53_zone" "primary" {
  name = "game.runf11s.com"
}

resource "aws_route53_record" "test" {
  zone_id = aws_route53_zone.primary.zone_id
  name    = "test.game.runf11s.com"
  type    = "A"
  ttl     = "300"
  records = ["8.8.8.8"]
}

output "name_servers" {
  value = aws_route53_zone.primary.name_servers
}

