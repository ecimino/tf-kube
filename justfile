default:
	echo 'Hello, world!'

validate:
	terraform validate

fmt:
	terraform fmt

destroy: fmt validate
	terraform destroy -auto-approve

apply: fmt validate
	terraform apply -auto-approve